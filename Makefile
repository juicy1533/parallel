default: 
	$(MAKE) -C ./single
	$(MAKE) -C ./openmp
	$(MAKE) -C ./thread
.PHONY: clean
clean: 
	rm -f single/*.o
