#include <cstdlib>
#include <cstdio>
#include <random>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <array>
#include <atomic>

#define NTHREADS 1
std::atomic<size_t> rows;

#ifdef OPENMP
#include <omp.h>
#endif
using namespace std;

//random_device rd;
//mt19937 gen(rd);
//uniform_real_distribution<double> uid(-1., 0.);
/*
1. Рандомизация матрицы - std::random_device;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution <double> uid(-1., 0.);
2. Замер времени;
3. Считывание из файла.
*/

class matrix {
	public double ** _data;
	public size_t _nrows, _ncols;

    void parse_file(ifstream &file) {
        size_t currow = 0;
        size_t curcol = 0;
        bool gotcols = false;
        _data = (double **) malloc(_nrows * sizeof(double *));

        string line;
        while (getline(file, line)) {
            istringstream stream(line);
            _data[currow] = (double *) malloc(_ncols * sizeof(double));

            while (!stream.eof()) {
                double n;
                stream >> n;
                if (gotcols && curcol >= _ncols) throw invalid_argument("matrix inputted incorrectly");

                _data[currow][curcol++] = n;
                if (!gotcols && curcol >= _ncols) {
                    _ncols <<= 1;
                    ++_ncols;
                    _data[currow] = (double *) realloc(_data[currow], _ncols * sizeof(double));
                }
            }

            if (!gotcols) {
                _ncols = curcol;
                _data[0] = (double *) realloc(_data[0], _ncols * sizeof(double));
                gotcols = true;
            }
            curcol = 0;
            ++currow;
            if (currow >= _nrows) {
                _nrows <<= 1;
                ++_nrows;
                _data = (double **) realloc(_data, _nrows * sizeof(double *));
            }
        }
        _nrows = currow;
        _data = (double **) realloc(_data, _nrows * sizeof(double *));

    }
public:
	
	matrix &copy() const {
		matrix *n = new matrix(this->_ncols, this->_nrows);
		for (size_t i = 0; i < _nrows; ++i) {
			for (size_t j = 0; j < _ncols; ++j) {
				(*n) [j][i] = (*this) [i][j];
			}
		}
		return (*n);
	}

	matrix(size_t nrows, size_t ncols) : _nrows(nrows), _ncols(ncols) {
		_data = (double **) aligned_alloc(32, nrows * sizeof(double *));
		for (size_t i = 0; i < nrows; ++i) {
			_data[i] = (double *) aligned_alloc(32, ncols * sizeof(double));
            		for (size_t j = 0; j < ncols; ++j) {
                		_data[i][j] = 0.;
            		}
		}
	}

    matrix(string sfile, size_t nrows = 7, size_t ncols = 7) : _nrows(nrows), _ncols(ncols) {
        ifstream file(sfile, ios::in);
        if (!file.is_open()) {
            throw invalid_argument("Cannot find specified file");
        }
        parse_file(file);
        file.close();
    }

    matrix(std::uniform_real_distribution<double> &d, std::mt19937 &generator, size_t nrows, size_t ncols) : _nrows(nrows), _ncols(ncols) {
        _data = (double **) aligned_alloc(32, _nrows * sizeof(double *));
        for (size_t i = 0; i < _nrows; ++i) {
            _data[i] = (double *) aligned_alloc(32, _ncols * sizeof(double));
            for (size_t j = 0; j < _ncols; ++j) {
                _data[i][j] = d(generator);
            }
        }
    }

	~matrix() {
		for (size_t i = 0; i < _nrows; ++i) {
			free(_data[i]);
		}
		free(_data);
	}
	
	size_t length(int axis) const {
		if (axis == 0) {
            return _nrows;
        } else if (axis == 1) {
            return _ncols;
        } else {
            char buff[50];
            snprintf(buff, sizeof(buff), "unknown axis '%d'", axis);
            throw invalid_argument(buff);
        }
	}

    double * operator[] (size_t row) const {
        if (row >= _nrows) {
            char buff[100];
            snprintf(buff, sizeof(buff), "index out of bounds with value '%lu' for axis of size %lu", row, _nrows);
            throw invalid_argument(buff);
        }
        return _data[row];
    }

	void print() const {
		for (size_t i = 0; i < _nrows; ++i) {
			for (size_t j = 0; j < _ncols; ++j) {
				cout << _data[i][j] << " ";
			}
			cout << endl;
		}
	}
	
	static void product(matrix &m, const matrix &left, const matrix &right, size_t num) {
		while(1) {
			auto i = rows++;
			if (i >= left.length(0)) break;
			for (size_t j = 0; j < right.length(1); ++j) {
				for (size_t k = 0; k < right.length(0); ++k) {
					m[i][j] += left[i][k] * right[j][k];
				}
			}
		}
	}

	__attribute__((noinline)) matrix &operator * (const matrix &left) const {
		if (left.length(1) != _nrows) throw invalid_argument("matrix dimensions forbid multiplication");

		matrix *result = new matrix(left.length(0), _ncols);

		//matrix right = this->copy();
		rows = 0;

		for (size_t i = 0; i < _nrows; ++i) {
			for (size_t j = i + 1; j < _ncols; ++j) {
				double temp = _data[i][j];
				_data[i][j] = _data[j][i];
				_data[j][i] = temp;
			}
		}
		/*
		

		array<thread, NTHREADS> threads;
		for (size_t i = 0; i < NTHREADS; ++i) {
			threads[i] = thread(product, ref(*result), cref(left), cref(right), i);
		}
		for (size_t i = 0; i < NTHREADS; ++i) {
			threads[i].join();
		}
        */
		

	/*
	#ifdef OPENMP
	int thrc = 4;
	omp_set_num_threads(thrc);
	cout << thrc << " threads" << endl;
    	#pragma omp parallel
    	{
    	#pragma omp for
    	#endif
	*/
	
    
	for (size_t i = 0; i < left.length(0); ++i) {
		for (size_t j = 0; j < _ncols; ++j) {
            //asm("LABEL:");
			/*__m256d *p1, *p2, v1, v2;
            double prod[4];
            p1 = reinterpret_cast<__m256d*>(left [i]);
            p2 = reinterpret_cast<__m256d*>(_data[j]);
            v1 = _mm256_sub_pd(v1, v1);
            for (size_t k = 0; k < _nrows; k += 4) {
                v2 = _mm256_mul_pd(*p1, *p2);
                v1 = _mm256_add_pd(v1, v2);
                p1++;
                p2++;
            }
            _mm256_store_pd(prod, v1);
            for (int ind = 0; ind < 4; ++ind) {
                (*result) [i][j] += prod[ind];
            }*/

            for (size_t k = 0; k < _nrows; ++k) {
				(*result) [i][j] += left[i][k] * _data[j][k];
			}
		}
	}
	
	/*
        #ifdef OPENMP
        }
        #endif
	*/
		return *result;
	}
};
