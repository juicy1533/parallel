#include <iostream>
#include "matrix.h"
#include <chrono>
#include <random>
#include <utility>
#include <cstdlib>
#include <cstring>
#include <mpi.h>
using namespace std;

int world_rank, world_size;

void run_slave() {
	while(true) {
		int command;
		MPI_Bcast(&command, 1, MPI_INT, 0, MPI_COMM_WORLD);
		if (command == 0) break;
		if (command == 1) {
			//command to multiply matrix
			size_t matrix_size;
			MPI_Bcast(&matrix_size, 1, MPI_INT, 0, MPI_COMM_WORLD);
			matrix b((size_t) matrix_size, (size_t) matrix_size);
			MPI_Bcast(b._data, matrix_size * matrix_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

			matrix m1((size_t) matrix_size, (size_t) matrix_size);
			MPI_Bcast(m1._data, )
			//MPI_Scatter(m1._data, matrix_size*matrix_size / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			
			matrix res((size_t) matrix_size, (size_t) matrix_size);
			for (size_t j = 0; j < matrix_size; ++j) {
				for (size_t k = 0; k < matrix_size; ++k) {
					res._data[i][j] += m1[i][k]*b[j][k];
				}
			}
		}
			
	}
}

int main(int argc, char **argv) {

	int size;
	cin >> size;
    
   /* 
    matrix m("matrix.inc");
    m.print();
    cout << endl;
    matrix r = m * m;
    r.print();*/
    
	#ifdef OPEN_MPI
		cout << "mpi working" << endl;
		MPI_Init(NULL, NULL);
		int world_size;
		MPI_Comm_size(MPI_COMM_WORLD, &world_size);
		if (world_size != 1) {
			int world_rank;
			MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
			if (world_rank) {
				run_slave(); // цикл готовности принимать задания
				return 0;
			}
			cout << world_rank << endl;
		}
		
		MPI_Finalize();
	#endif
	random_device device;
	mt19937 generator(device());
	uniform_real_distribution<double> next(0., 10.);

	matrix l(next, generator, size, size);
	matrix r(next, generator, size, size);

	auto start = chrono::steady_clock::now();

	matrix res = r * l;

	auto end = chrono::steady_clock::now();
	auto diff = end - start;
	int time = chrono::duration_cast<chrono::milliseconds>(diff).count();
	cout << "Done in " << time << "ms" << endl;
	
	return 0;
}
